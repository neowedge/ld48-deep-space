﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts
{
    public static class ColorExtensions
    {
        public static void SetAlpha(this Color color, float alpha)
        {
            color.a = alpha;
        }
    }
}