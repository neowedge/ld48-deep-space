﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts
{
    public class WorldActor : MonoBehaviour
    {
        private Transform _t;

        public Transform T {
            get {
                if (_t == null)
                {
                    _t = GetComponent<Transform>();
                }
                return _t;
            }
        }
    }
}