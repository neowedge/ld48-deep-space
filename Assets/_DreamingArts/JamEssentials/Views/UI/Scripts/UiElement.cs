﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts
{
    public class UiElement : MonoBehaviour
    {
        private RectTransform _t;


        public RectTransform T {
            get {
                if (_t == null)
                {
                    _t = GetComponent<RectTransform>();
                }
                return _t;
            }
        }
    }
}