using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace com.DreamingArts.DeepSpace
{
    public interface IPickScreen
    {
        GameManager GameManager { get; }
        void Pick(Item item, int index);
    }
}