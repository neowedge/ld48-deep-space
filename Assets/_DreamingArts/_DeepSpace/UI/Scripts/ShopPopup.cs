using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class ShopPopup : PopUp, IPickScreen
    {
        [SerializeField]
        ItemPanel[] _rewardsPanels;

        [SerializeField]
        CanvasGroup _notEnoughCreditsPanel;

        GameManager _gameManager;
        Location _location;

        public GameManager GameManager => _gameManager;

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void Show(Location location)
        {
            _location = location;
            Show();
        }

        public override bool Open()
        {
            if (base.Open())
            {
                for (int i = 0; i < _rewardsPanels.Length; ++i)
                {
                    _rewardsPanels[i].Setup(this,
                        _location.Items[i].item,
                        i,
                        false,
                        _location.Items[i].sold);
                }

                return true;
            }
            return true;
        }

        public void Pick(Item item, int index)
        {
            int price = item.GetPrice(_gameManager, _gameManager.Player);
            if (price <= _gameManager.Credits)
            {
                _gameManager.Credits -= price;
                _location.Items[index].sold = true;

                for (int i = 0; i < _rewardsPanels.Length; ++i)
                {
                    _rewardsPanels[i].Setup(this,
                        _location.Items[i].item,
                        i,
                        false,
                        _location.Items[i].sold);
                }
            }
            else
            {
                DOTween.Kill(_notEnoughCreditsPanel);
                _notEnoughCreditsPanel.gameObject.SetActive(true);
                DOTween.Sequence()
                    .Append(_notEnoughCreditsPanel.DOFade(1, 0.1f))
                    .AppendInterval(1.5f)
                    .Append(_notEnoughCreditsPanel.DOFade(0, 0.1f));
            }
        }
    }
}