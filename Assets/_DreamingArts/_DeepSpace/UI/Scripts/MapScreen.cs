using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class MapScreen : Screen
    {
        [SerializeField]
        CanvasGroup _canvasGroup;

        [SerializeField]
        private LocationPanel[] _locationPanels;

        private GameManager _gameManager;

        [SerializeField]
        Vector2 _locationOffset;

        [SerializeField]
        RectTransform _playerPointer;

        public Vector2 LocationOffset => _locationOffset;

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;

            _canvasGroup.alpha = 0;
            _canvasGroup.gameObject.SetActive(false);

            for (int i = 0; i < _locationPanels.Length; ++i)
            {
                _locationPanels[i].Setup(this);
            }

            _gameManager.LocationsManager.OnGenerateLocations += LocationsManager_OnGenerateLocations;
        }

        public override bool Open()
        {
            if (base.Open())
            {
                _canvasGroup.gameObject.SetActive(true);
                _canvasGroup.DOFade(1f, 0.5f);

                return true;
            }
            return false;
        }

        public override bool Close()
        {
            if (_isOpen)
            {
                _isOpen = false;

                _canvasGroup.DOFade(0, 0.5f);

                return true;
            }
            return false;
        }

        private void LocationsManager_OnGenerateLocations()
        {
            SetMap();
        }

        public void SetMap()
        {
            _playerPointer.anchoredPosition = new Vector2(-400f, 0);

            for (int i = 0; i < _locationPanels.Length; ++i)
            {
                _locationPanels[i].Init(_gameManager.LocationsManager.Locations[i]);
            }
        }

        public void SetPointerPosition(Vector2 position)
        {
            _playerPointer.position = position;
        }

        public void VisitLocation(Location _location)
        {
            Debug.Log(_location.Type);

            switch (_location.Type)
            {
                case Location.ELocationType.Commerce:
                    _gameManager.ViewManager.ShowShopPopup(_location);
                    break;

                case Location.ELocationType.Combat:
                    _gameManager.ViewManager.ShowCombatScreen();
                    break;

                case Location.ELocationType.WarpRing:
                    _gameManager.ViewManager.ShowConfirmTravel();
                    break;
            }
        }
    }
}