using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class JumpPopup : PopUp
    {
        GameManager _gameManager;

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void Travel()
        {
            Hide();
            _gameManager.ViewManager.JumpToNextSector();
        }
    }
}