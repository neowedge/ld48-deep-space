using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace com.DreamingArts.DeepSpace
{
    public class LocationPanel : UiElement
    {
        [SerializeField]
        RectTransform _locationPicture;

        [SerializeField]
        Localize _locationText;

        [SerializeField]
        GridLayout _grid;

        MapScreen _screen;
        private Location _location;

        private void OnDestroy()
        {
            if (_location != null)
            {
                _location.OnView -= Location_OnView;
                _location.OnVisit -= Location_OnVisit;
            }
        }

        public void Setup(MapScreen screen)
        {
            _screen = screen;
        }

        public void Init(Location location)
        {
            if (_location != null)
            {
                _location.OnView -= Location_OnView;
                _location.OnVisit -= Location_OnVisit;
            }

            _location = location;
            SetPosition();

            _locationPicture.gameObject.SetActive(_location.Viewed);

            _locationText.SetTerm("Location_Unknown");

            _location.OnVisit += Location_OnVisit;
            _location.OnView += Location_OnView;
        }

        private void Location_OnVisit()
        {
            switch (_location.Type)
            {
                case Location.ELocationType.Combat:
                    _locationPicture.gameObject.SetActive(false);
                    break;

                case Location.ELocationType.Commerce:
                    _locationText.SetTerm("Location_CommerceStation");
                    break;

                case Location.ELocationType.WarpRing:
                    _locationText.SetTerm("Location_WarpRing");
                    break;
            }

            _screen.SetPointerPosition(_locationPicture.position);
        }

        private void Location_OnView()
        {
            _location.OnView -= Location_OnView;
            _locationPicture.gameObject.SetActive(true);
        }

        [Button]
        public void SetPosition()
        {
            _locationPicture.anchoredPosition = new Vector2(
                Random.Range(-_screen.LocationOffset.x, _screen.LocationOffset.x),
                Random.Range(-_screen.LocationOffset.y, _screen.LocationOffset.y));
        }

        public void VisitLocation()
        {
            _location.Visit();
            _screen.VisitLocation(_location);
        }
    }
}