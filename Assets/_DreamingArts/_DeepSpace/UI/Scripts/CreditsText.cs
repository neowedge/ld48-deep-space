using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class CreditsText : MonoBehaviour
    {
        [SerializeField]
        private GameManager _gameManager;

        [SerializeField]
        private TMP_Text _text;

        private void OnDestroy()
        {
            _gameManager.OnCreditsChange -= GameManager_OnCreditsChange;
        }

        void Start()
        {
            SetValue();
            _gameManager.OnCreditsChange += GameManager_OnCreditsChange;
        }

        private void GameManager_OnCreditsChange()
        {
            SetValue();
        }

        private void SetValue()
        {
            _text.text = _gameManager.Credits.ToString() + " Cr.";
        }
    }
}