using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class SectorText : MonoBehaviour
    {
        [SerializeField]
        private GameManager _gameManager;

        [SerializeField]
        private TMP_Text _text;

        private void OnDestroy()
        {
            _gameManager.LocationsManager.OnGenerateLocations -= LocationsManager_OnGenerateLocations;
        }

        void Start()
        {
            SetValue();
            _gameManager.LocationsManager.OnGenerateLocations += LocationsManager_OnGenerateLocations;
        }

        private void LocationsManager_OnGenerateLocations()
        {
            SetValue();
        }

        private void SetValue()
        {
            _text.text = "Sector " + _gameManager.LocationsManager.CurrentSector.ToString();
        }
    }
}