using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class CombatRewardPopup : PopUp, IPickScreen
    {
        [SerializeField]
        Item _mandatoryReward;

        [SerializeField]
        Item[] _randomRewards;

        [SerializeField]
        ItemPanel[] _rewardsPanels;

        Combat _combat;
        GameManager _gameManager;

        public Combat Combat => _combat;
        public GameManager GameManager => _gameManager;

        private bool _rewardTaken;

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void Show(Combat combat)
        {
            _combat = combat;
            _rewardTaken = false;
            base.Show();
        }

        public override bool Open()
        {
            if (base.Open())
            {
                _mandatoryReward.SetValue(_combat.Credits);

                List<Item> possibleRewards = new List<Item>(_randomRewards);

                _rewardsPanels[0].Setup(this, _mandatoryReward, 0);
                for (int i = 1; i < _rewardsPanels.Length; ++i)
                {
                    int pick = Random.Range(0, possibleRewards.Count);

                    _rewardsPanels[i].Setup(this,
                        possibleRewards[pick],
                        i);

                    possibleRewards.RemoveAt(pick);
                }

                return true;
            }
            return true;
        }

        public void Pick(Item item, int index)
        {
            if (!_rewardTaken)
            {
                _rewardTaken = true;
                item.Take(_gameManager, _gameManager.Player);
                Hide();
                _combat.Screen.Jump();
            }
        }
    }
}