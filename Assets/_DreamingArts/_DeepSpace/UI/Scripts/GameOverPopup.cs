using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace com.DreamingArts.DeepSpace
{
    public class GameOverPopup : PopUp
    {
        [SerializeField]
        TMP_Text _tapToContinue;

        CoroutineHandle _checkInputCoroutine;

        public override bool Open()
        {
            _tapToContinue.gameObject.SetActive(false);
            _checkInputCoroutine = Timing.CallDelayed(2f, BeginCheckInput);
            return base.Open();
        }

        private void BeginCheckInput()
        {
            _tapToContinue.gameObject.SetActive(true);
            _checkInputCoroutine = Timing.CallContinuously(Mathf.Infinity, CheckInput);
        }

        private void CheckInput()
        {
            if (Input.anyKeyDown)
            {
                Timing.KillCoroutines(_checkInputCoroutine);
                SceneManager.LoadScene(0);
            }
        }
    }
}