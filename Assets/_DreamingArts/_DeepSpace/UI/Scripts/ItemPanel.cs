using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace com.DreamingArts.DeepSpace
{
    public class ItemPanel : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text _titleText;

        [SerializeField]
        private TMP_Text _descriptionText;

        [SerializeField]
        private TMP_Text _priceText;

        [SerializeField]
        private Button _pickButton;

        [SerializeField]
        private Color _freeColor = Color.white;

        [SerializeField]
        private Color _payColor = Color.white;

        [SerializeField]
        private RectTransform _soldPanel;

        int _index;
        Item _item;
        IPickScreen _screen;

        public void Setup(IPickScreen screen, Item item, int index, bool isFree = true, bool isSold = false)
        {
            _screen = screen;
            _item = item;
            _index = index;

            _titleText.text = _item.GetTitle();
            _descriptionText.text = _item.GetDescription(_screen.GameManager, _screen.GameManager.Player);
            _descriptionText.color = _item.Color;
            if (isFree)
            {
                _priceText.text = I2.Loc.LocalizationManager.GetTranslation("Item_Free");
                _priceText.color = _freeColor;
            }
            else
            {
                _priceText.text = _item.GetPrice(_screen.GameManager, _screen.GameManager.Player).ToString() + "Cr.";
                _priceText.color = _payColor;
            }

            _pickButton.interactable = !isSold && _item.GetValue(_screen.GameManager, _screen.GameManager.Player) > 0;
            _soldPanel.gameObject.SetActive(isSold);
        }

        public void Pick()
        {
            _screen.Pick(_item, _index);
        }
    }
}