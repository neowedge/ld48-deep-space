using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class ViewManager : MonoBehaviour
    {
        [SerializeField]
        private Transform _mainCameraTransform;

        [SerializeField]
        private HyperspaceEffect _hyperspaceEffect;

        [SerializeField]
        private MapScreen _mapScreen;

        [SerializeField]
        private CombatScreen _combatScreen;

        [SerializeField]
        private CombatRewardPopup _combatRewardsPopup;

        [SerializeField]
        private ShopPopup _shopPopup;

        [SerializeField]
        private JumpPopup _jumpPupup;

        [SerializeField]
        private GameOverPopup _gameOverPopup;

        GameManager _gameManager;

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;

            _mapScreen.Setup(gameManager);
            _combatScreen.Setup(_gameManager);
            _combatRewardsPopup.Setup(_gameManager);
            _shopPopup.Setup(_gameManager);
            _jumpPupup.Setup(_gameManager);
        }

        public void ShowMapScreen()
        {
            _mapScreen.Show();
        }

        public void ShowCombatScreen()
        {
            _mapScreen.Hide();
            _combatScreen.Show();
        }

        public void ShowCombatRewardPopup(Combat combat)
        {
            _combatRewardsPopup.Show(combat);
        }

        public void ShowShopPopup(Location location)
        {
            _shopPopup.Show(location);
        }

        public void ShowConfirmTravel()
        {
            _jumpPupup.Show();
        }

        public void JumpToNextSector()
        {
            _mapScreen.Hide();
            Jump(() =>
            {
                _gameManager.LocationsManager.JumpToNextSector();
                _mapScreen.Show();
            });
        }

        public void ShowGameOver()
        {
            _gameOverPopup.Show();
        }

        // Update is called once per frame
        public void Jump(TweenCallback callback)
        {
            _hyperspaceEffect.Play(callback);
        }

        [Button]
        public void ScreenShake()
        {
            _mainCameraTransform.DOShakePosition(0.1f, 0.3f, 50, 90, false).OnComplete(() => {
                    _mainCameraTransform.position = new Vector3(0, 0, -10f);
            });
        }

        [Button]
        public void BigScreenShake()
        {
            _mainCameraTransform.DOShakePosition(0.5f, 0.8f, 100, 90, false).OnComplete(() => {
                    _mainCameraTransform.position = new Vector3(0, 0, -10f);
            });
        }
    }
}