using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using I2.Loc;
using MEC;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.ProceduralImage;

namespace com.DreamingArts.DeepSpace
{
    public class CombatScreen : Screen
    {
        [SerializeField]
        private Vector3 _playerPosition;

        [SerializeField]
        private Vector3[] _enemiesPositions;

        [SerializeField]
        private CanvasGroup _canvasGroup;

        [SerializeField]
        private Button _jumpButton;

        [SerializeField]
        private LocalizationParamsManager _damageLocalizationParams;

        [SerializeField]
        private LocalizationParamsManager _attackLocalizationParams;

        [SerializeField]
        private ProceduralImage _jumpSlider;

        [SerializeField]
        private ProceduralImage _shieldSlider;

        [SerializeField]
        private ProceduralImage _hullSlider;

        [SerializeField]
        private ProceduralImage _attackSlider;

        public Vector3 PlayerPosition => _playerPosition;
        public Vector3[] EnemiesPositions => _enemiesPositions;

        private GameManager _gameManager;
        private Combat _combat;

        public GameManager GameManager => _gameManager;

        CoroutineHandle _updateCoroutine;

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;

            _canvasGroup.alpha = 0;
        }

        public override bool Open()
        {
            if (base.Open())
            {
                _canvasGroup.gameObject.SetActive(true);
                _canvasGroup.DOFade(1f, 0.2f);
                InitCombat();
                return true;
            }

            return false;
        }

        public override bool Close()
        {
            if (_isOpen)
            {
                _canvasGroup.DOFade(0, 0.2f).OnComplete(() =>
                {
                    _canvasGroup.gameObject.SetActive(false);
                    base.Close();
                });
                return true;
            }
            return false;
        }

        public void InitCombat()
        {
            _combat = new Combat(_gameManager, this);

            _jumpButton.interactable = false;

            _hullSlider.fillAmount = _gameManager.Player.HullNormalized;

            _damageLocalizationParams.SetParameterValue("HULL", _gameManager.Player.CurrentHull.ToString());
            _damageLocalizationParams.SetParameterValue("SHIELD", _gameManager.Player.CurrentShield.ToString("F0"));
            _attackLocalizationParams.SetParameterValue("DAMAGE", _gameManager.Player.DamagePerSecond.ToString("F0"));

            _updateCoroutine = Timing.CallContinuously(Mathf.Infinity, UpdateValues);

            _gameManager.Player.OnCurrentShieldChange += Player_OnCurrentShieldChange;
            _gameManager.Player.OnCurrentHullChange += Player_OnCurrentHullChange;
        }


        private void UpdateValues()
        {
            if (!_jumpButton.interactable)
            {
                if (_gameManager.Player.HyperspaceRechargeNormalized > 1f)
                {
                    _jumpSlider.fillAmount = 1f;
                    _jumpButton.interactable = true;
                }
                else
                {
                    _jumpSlider.fillAmount = _gameManager.Player.HyperspaceRechargeNormalized;
                }
            }
            _attackSlider.fillAmount = _gameManager.Player.AttackRechargeNormalized;
        }

        private void Player_OnCurrentShieldChange()
        {
            _damageLocalizationParams.SetParameterValue("SHIELD", _gameManager.Player.CurrentShield.ToString("F0"));
            _shieldSlider.fillAmount = _gameManager.Player.ShieldNormalized;
        }

        private void Player_OnCurrentHullChange()
        {
            _damageLocalizationParams.SetParameterValue("HULL", _gameManager.Player.CurrentHull.ToString());
            _hullSlider.fillAmount = _gameManager.Player.HullNormalized;
        }

        public void Jump()
        {
            Timing.KillCoroutines(_updateCoroutine);

            _combat.Dispose();
            _combat = null;
            Hide();
            _gameManager.ViewManager.Jump(_gameManager.ViewManager.ShowMapScreen);
        }

        public void EndCombat()
        {
            Timing.KillCoroutines(_updateCoroutine);
            _jumpButton.interactable = false;

            Timing.CallDelayed(1.5f, () => {
                _gameManager.ViewManager.ShowCombatRewardPopup(_combat);
            });
        }

        internal void GameOver()
        {
            Timing.KillCoroutines(_updateCoroutine);
            _jumpButton.interactable = false;

            Timing.CallDelayed(1.5f, () => {
                _gameManager.ViewManager.ShowGameOver();
            });
        }
    }
}