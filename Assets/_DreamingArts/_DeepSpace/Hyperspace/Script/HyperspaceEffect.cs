using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class HyperspaceEffect : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _hyperspaceParticleSystem;

    [SerializeField]
    private SpriteRenderer _backgroundRenderer;

    [Button]
    public void Play(TweenCallback callback)
    {
        _hyperspaceParticleSystem.Play();
        DOTween.Sequence()
            .Append(_backgroundRenderer.DOFade(0, 0.1f))
            .AppendInterval(2f)
            .Append(_backgroundRenderer.DOFade(1f, 0.1f))
            .OnComplete(callback);
            
    }
}
