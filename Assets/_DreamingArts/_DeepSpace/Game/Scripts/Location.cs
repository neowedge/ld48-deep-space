using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class Location
    {
        public enum ELocationType
        {
            Commerce,
            Combat,
            WarpRing
        }

        public struct ItemState
        {
            public Item item;
            public bool sold;
        }

        private ELocationType _type;
        private int _index;
        private Vector2Int _cellPos;

        private bool _visited;
        private bool _viewed;

        private ItemState[] _items;

        GameManager _gameManager;
        public ELocationType Type => _type;
        public bool Visited => _visited;
        public bool Viewed => _viewed;
        public ItemState[] Items => _items;

        public event System.Action OnVisit;
        public event System.Action OnView;

        public Location(GameManager gameManager, ELocationType type, int index)
        {
            _gameManager = gameManager;
            _type = type;
            _visited = false;
            _viewed = false;
            _index = index;
            _cellPos = GetCellPosition();

            if (_cellPos.x == 0)
            {
                View();
            }

            if (_type == ELocationType.Commerce)
            {
                _items = new ItemState[3];

                _items[0] = new ItemState{
                    item = gameManager.LocationsManager.MandatoryCommerceItem,
                    sold = false
                };
                for (int i = 1; i < _items.Length; ++i)
                {
                    _items[i] = new ItemState
                    {
                        item = gameManager.LocationsManager.PossibleCommerceItem[
                            Random.Range(0, gameManager.LocationsManager.PossibleCommerceItem.Length)],
                        sold = false
                    }; 
                }       
            }
        }

        public void CheckViewFrom(Vector2Int cellPos)
        {
            if ((_cellPos.x == cellPos.x &&
                Mathf.Abs(_cellPos.y - cellPos.y) < 2) ||
                (_cellPos.y == cellPos.y &&
                Mathf.Abs(_cellPos.x - cellPos.x) < 2))
            {
                View();
            }
        }

        public void View()
        {
            _viewed = true;
            OnView?.Invoke();
        }

        public void Visit()
        {
            if (!_visited)
            {
                _visited = true;
                for (int i = 0; i < _gameManager.LocationsManager.Locations.Length; ++i)
                {
                    if (i != _index)
                    {
                        _gameManager.LocationsManager.Locations[i].CheckViewFrom(_cellPos);
                    }
                }
            }
            OnVisit?.Invoke();

            // Begin event
        }

        public Vector2Int GetCellPosition()
        {
            return new Vector2Int(
                _index % 4,
                _index / 4);
        }
    }
}