using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class ShipModel : ScriptableObject
    {
        [BoxGroup("Prefab")]
        public Ship prefab;

        [BoxGroup("Hull")]
        public int baseHull = 100;

        [BoxGroup("Hull")]
        public int hullLevelIncrement = 50;

        [BoxGroup("Shield")]
        public int baseShield = 50;

        [BoxGroup("Shield")]
        public int shieldLevelIncrement = 25;

        [BoxGroup("Shield")]
        public float baseShieldRechargeSpeed = 2f;

        [BoxGroup("Shield")]
        public float shieldRechargeSpeedLevelImprove = 2f;

        [BoxGroup("Shield")]
        public float baseShieldRechargeCooldown = 5f;

        [BoxGroup("Shield")]
        public float shieldRechargeCooldownLevelImprove = 0.1f;

        [BoxGroup("Attack")]
        public int baseAttackDamage = 20;

        [BoxGroup("Attack")]
        public int attackDamageLevelIncrement = 20;

        [BoxGroup("Attack")]
        public float baseAttackCooldown = 2f;

        [BoxGroup("Attack")]
        public float attackCooldownLevelImprove = 0.1f;

        [BoxGroup("Hyperspace")]
        public float baseHyperspaceCooldown = 30f;

        [BoxGroup("Hyperspace")]
        public float hyperspaceCooldownLevelImprove = 0.1f;
    }
}