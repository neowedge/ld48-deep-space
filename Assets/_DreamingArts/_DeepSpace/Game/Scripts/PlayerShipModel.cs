using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    [CreateAssetMenu(fileName = "Ship", menuName = "Deep Space/PlayerShip")]
    public class PlayerShipModel : ShipModel
    {
        
    }
}