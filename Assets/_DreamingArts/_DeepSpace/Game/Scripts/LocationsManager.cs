using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class LocationsManager : MonoBehaviour
    {
        [SerializeField]
        private EnemyShipModel[] _enemies;

        [SerializeField]
        int _minCommerce = 2;

        [SerializeField]
        int _maxCommerce = 4;

        [SerializeField]
        Item _mandatoryCommerceItem;

        [SerializeField]
        Item[] _possibleCommerceItem;

        private GameManager _gameManager;

        private int _currentSystemLevel = 1;

        private Location[] _locations;

        public EnemyShipModel[] Enemies => _enemies;

        public int CurrentSector => _currentSystemLevel;

        public Item MandatoryCommerceItem => _mandatoryCommerceItem;

        public Item[] PossibleCommerceItem => _possibleCommerceItem;

        public event System.Action OnGenerateLocations;

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public Location[] Locations => _locations;

        public void Init()
        {
            GenerateLocations();
        }

        public List<EnemyShipController> GetEnemies()
        {
            List<EnemyShipModel> possibleEnemies = new List<EnemyShipModel>();

            for (int i = 0; i < _enemies.Length; ++i)
            {
                if (_enemies[i].minLocationLevel <= _currentSystemLevel)
                {
                    possibleEnemies.Add(_enemies[i]);
                }
            }

            int countEnemies;
            if (_currentSystemLevel < 3)
            {
                countEnemies = Random.Range(1, 3);
            }
            else if (_currentSystemLevel < 5)
            {
                countEnemies = Random.Range(1, 4);
            }
            else if(_currentSystemLevel < 7)
            {
                countEnemies = Random.Range(2, 4);
            }
            else
            {
                countEnemies = 3;
            }

            List<EnemyShipController> enemies = new List<EnemyShipController>(countEnemies);

            for (int i = 0; i < countEnemies; ++i)
            {
                enemies.Add(new EnemyShipController(
                        _gameManager,
                        possibleEnemies[Random.Range(0, possibleEnemies.Count)], _currentSystemLevel));
            }

            return enemies;
        }

        public void JumpToNextSector()
        {
            ++_currentSystemLevel;
            GenerateLocations();
        }

        private void GenerateLocations()
        {
            List<Location.ELocationType> possibleLocations = new List<Location.ELocationType>();
            possibleLocations.Add(Location.ELocationType.WarpRing);
            int commerces = Random.Range(_minCommerce, _maxCommerce);
            int i = 1;
            for (; i <= commerces; ++i)
            {
                possibleLocations.Add(Location.ELocationType.Commerce);
            }

            for (; i < 12; ++i)
            {
                possibleLocations.Add(Location.ELocationType.Combat);
            }

            _locations = new Location[12];

            for (i = 0; i < 12; ++i)
            {
                int pick = Random.Range(0, possibleLocations.Count);
                _locations[i] = new Location(_gameManager, possibleLocations[pick], i);
                possibleLocations.RemoveAt(pick);
            }


            OnGenerateLocations?.Invoke();
        }
    }
}
