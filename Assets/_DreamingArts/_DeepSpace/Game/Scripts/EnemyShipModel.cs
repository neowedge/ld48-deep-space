using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    [CreateAssetMenu(fileName = "Ship", menuName = "Deep Space/Enemy Ship")]
    public class EnemyShipModel : ShipModel
    {
        [BoxGroup("Reward")]
        public int baseScrapReward;

        [BoxGroup("Reward")]
        public int scrapRewardLevelIncrement;

        [BoxGroup("Unlock")]
        public int minLocationLevel;

    }
}