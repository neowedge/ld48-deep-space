using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class PlayerShipController : ShipController
    {
        public PlayerShipController(GameManager gameManager, PlayerShipModel model) : base(gameManager, model)
        {
            CurrentHull = Hull;
        }

        public override void InitCombat(Combat combat, Ship ship)
        {
            _combat = combat;
            _ship = ship;

            CurrentShield = Shield;

            _combatBeginTime = Time.time;
            _hyperspaceCooldownEndTime = Time.time + HyperspaceCooldown;
            _shotCooldownEndTime = Time.time + AttackCooldown;

            _updateCoroutine = Timing.CallContinuously(Mathf.Infinity, Update);
        }

    }
}