using System.Collections;
using System.Collections.Generic;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public abstract class ShipController
    {
        protected ShipModel _model;

        protected int _hullLevel = 1;
        protected int _shieldLevel = 1;
        protected int _shieldRechargeSpeedLevel = 1;
        protected int _shieldRechargeCooldownLevel = 1;
        protected int _attackDamageLevel = 1;
        protected int _attackCooldownLevel = 1;
        protected int _hyperspaceCooldownLevel = 1;

        protected int _currentHull;
        protected float _currentShield;

        protected float _combatBeginTime;
        protected float _shotCooldownEndTime;
        protected float _hyperspaceCooldownEndTime;
        protected float _shieldRechargeCooldownEndTime;

        private GameManager _gameManager;

        [ShowInInspector, InlineEditor, HideInEditorMode, BoxGroup("Game State")]
        protected Combat _combat;

        protected Ship _ship;

        protected ShipController _target;

        protected CoroutineHandle _updateCoroutine;

        public int HullLevel
        {
            get => _hullLevel;
            set
            {
                if (value != _hullLevel)
                {
                    _hullLevel = value;
                    OnHullLevelChange?.Invoke();
                }
            }
        }

        public int ShieldLevel
        {
            get => _shieldLevel;
            set
            {
                if (value != _shieldLevel)
                {
                    _shieldLevel = value;
                    OnShieldLevelChange?.Invoke();
                }
            }
        }

        public int ShieldRechargeSpeedLevel
        {
            get => _shieldRechargeSpeedLevel;
            set
            {
                if (value != _shieldRechargeSpeedLevel)
                {
                    _shieldRechargeSpeedLevel = value;
                    OnShieldRechargeSpeedLevelChange?.Invoke();
                }
            }
        }

        public int ShieldRechargeCooldownLevel
        {
            get => _shieldRechargeCooldownLevel;
            set
            {
                if (value != _shieldRechargeCooldownLevel)
                {
                    _shieldRechargeCooldownLevel = value;
                    OnShieldRechargeCooldownLevelChange?.Invoke();
                }
            }
        }

        public int AttackDamageLevel
        {
            get => _attackDamageLevel;
            set
            {
                if (value != _attackDamageLevel)
                {
                    _attackDamageLevel = value;
                    OnAttackDamageLevelChange?.Invoke();
                }
            }
        }

        public int AttackCooldownLevel
        {
            get => _attackCooldownLevel;
            set
            {
                if (value != _attackCooldownLevel)
                {
                    _attackCooldownLevel = value;
                    OnAttackCooldownLevelChange?.Invoke();
                }
            }
        }

        public int HyperspaceCooldownLevel
        {
            get => _hyperspaceCooldownLevel;
            set
            {
                if (value != _hyperspaceCooldownLevel)
                {
                    _hyperspaceCooldownLevel = value;
                    OnHyperspaceCooldownLevelChange?.Invoke();
                }
            }
        }


        public int Hull => _model.baseHull + ((_hullLevel - 1) * _model.hullLevelIncrement);
        public int Shield => _model.baseShield + ((_shieldLevel - 1) * _model.shieldLevelIncrement);
        public float ShieldRechargeSpeed => _model.baseShieldRechargeSpeed + ((_shieldLevel - 1) * _model.shieldLevelIncrement);
        public float ShieldRechargeCooldown => _model.baseShieldRechargeCooldown / (1 + ((_shieldLevel - 1) * _model.shieldRechargeCooldownLevelImprove));
        public int AttackDamage => _model.baseAttackDamage + ((_attackDamageLevel - 1) * _model.attackDamageLevelIncrement);
        public float AttackCooldown => _model.baseAttackCooldown / (1 + ((_shieldLevel - 1) * _model.attackCooldownLevelImprove));
        public float HyperspaceCooldown => _model.baseHyperspaceCooldown / (1 + ((_shieldLevel - 1) * _model.hyperspaceCooldownLevelImprove));

        public int CurrentHull
        {
            get => _currentHull;
            set
            {
                value = Mathf.Clamp(value, 0, Hull);
                if (value != _currentHull)
                {
                    _currentHull = value;
                    OnCurrentHullChange?.Invoke();
                }
            }
        }

        public float CurrentShield
        {
            get => _currentShield;
            set
            {
                value = Mathf.Clamp(value, 0, Shield);
                if (value != _currentShield)
                {
                    _currentShield = value;
                    OnCurrentShieldChange?.Invoke();
                }
            }
        }

        public float DamagePerSecond => AttackDamage / AttackCooldown;

        public Combat Combat => _combat;
        public ShipModel Model => _model;
        public Ship Ship => _ship;
        public ShipController Target => _target;

        public float CombatBeginTime => _combatBeginTime;
        public float HyperspaceCooldownEndTime => _hyperspaceCooldownEndTime;

        public float HyperspaceRechargeNormalized => (Time.time - _combatBeginTime) / HyperspaceCooldown;
        public float AttackRechargeNormalized => (Time.time - (_shotCooldownEndTime - AttackCooldown)) / AttackCooldown;
        public float ShieldNormalized => _currentShield / Shield;
        public float HullNormalized => (float)_currentHull / (float)Hull;

        public event System.Action OnHullLevelChange;
        public event System.Action OnShieldLevelChange;
        public event System.Action OnShieldRechargeSpeedLevelChange;
        public event System.Action OnShieldRechargeCooldownLevelChange;
        public event System.Action OnAttackDamageLevelChange;
        public event System.Action OnAttackCooldownLevelChange;
        public event System.Action OnHyperspaceCooldownLevelChange;

        public event System.Action OnCurrentHullChange;
        public event System.Action OnCurrentShieldChange;

        public ShipController(GameManager gameManager, ShipModel model)
        {
            _gameManager = gameManager;
            _model = model;
        }

        public virtual void InitCombat(Combat combat, Ship ship)
        {
            _combat = combat;
            _ship = ship;

            CurrentHull = Hull;
            CurrentShield = Shield;

            _combatBeginTime = Time.time;
            _hyperspaceCooldownEndTime = Time.time + HyperspaceCooldown;
            _shotCooldownEndTime = Time.time + AttackCooldown * Random.Range(0.75f, 1f);

            _updateCoroutine = Timing.CallContinuously(Mathf.Infinity, Update);
        }

        public virtual void EndCombat()
        {
            Timing.KillCoroutines(_updateCoroutine);
        }

        public void SetTarget(ShipController target)
        {
            _target = target;
        }

        public void Dispose()
        {
            Timing.KillCoroutines(_updateCoroutine);
        }

        public virtual void Attack()
        {
            _shotCooldownEndTime = Time.time + AttackCooldown;
            _target.TakeDamage(AttackDamage, _ship.Shot(_target.Ship));
        }

        public void TakeDamage(int damage, Vector3 impactOffset)
        {
            if (_currentShield < damage)
            {
                damage -= (int)_currentShield;

                CurrentShield = 0;
                CurrentHull -= damage;

                if (_currentHull <= 0)
                {
                    Destroy();
                }
                else
                {
                    _ship.TakeHullDamage(impactOffset);
                }
            }
            else
            {
                CurrentShield -= damage;
                _ship.Shield();
            }

            _shieldRechargeCooldownEndTime = Time.time + ShieldRechargeCooldown;
        }

        public void Destroy()
        {
            Timing.KillCoroutines(_updateCoroutine);
            _ship.Destroy();
        }

        protected virtual void Update()
        {
            if (Time.time >= _shieldRechargeCooldownEndTime)
            {
                CurrentShield += ShieldRechargeSpeed * Time.deltaTime;
            }

            if (Time.time >= _shotCooldownEndTime)
            {
                Attack();
            }
        }
    }
}