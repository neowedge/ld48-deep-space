using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class EnemyShipController : ShipController
    {
        public int CreditsReward => ((EnemyShipModel)_model).baseScrapReward + ((_shieldLevel - 1) * ((EnemyShipModel)_model).scrapRewardLevelIncrement);

        public EnemyShipController(GameManager gameManager, EnemyShipModel model, int level) : base(gameManager, model)
        {
            HullLevel = level;
            ShieldLevel = level;
            ShieldRechargeSpeedLevel = level;
            ShieldRechargeCooldownLevel = level;
            AttackDamageLevel = level;
            AttackCooldownLevel = level;
            HyperspaceCooldownLevel = level;
        }
    }
}