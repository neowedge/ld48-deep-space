using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class Ship : WorldActor
    {
        [SerializeField]
        private SpriteRenderer _renderer;

        [SerializeField]
        private LineRenderer[] _laserBeams;

        [SerializeField]
        private SpriteRenderer _shield;

        [SerializeField]
        private ExplossionEffect _explossionEffect;

        private ShipController _shipController;

        public void OnDestroy()
        {
            DOTween.Kill(_shield);
        }

        public void Setup(ShipController shipController)
        {
            _shipController = shipController;
        }

        [Button]
        public Vector3 Shot(Ship ship)
        {
            Vector3 impactOffset = new Vector3(
                Random.Range(-0.3f, 0.3f),
                Random.Range(-0.7f, 0.7f),
                0);

            Vector3 impactPosition = ship.T.position - T.position + impactOffset;

            for (int i = 0; i < _laserBeams.Length; ++i)
            {
                Vector3 beamOriginPosition = _laserBeams[i].GetPosition(0);
                Vector3 beamImpactPosition = impactPosition;

                beamImpactPosition.x += beamOriginPosition.x / 3f;
                beamImpactPosition.z = beamOriginPosition.z;

                _laserBeams[i].gameObject.SetActive(true);
                _laserBeams[i].SetPosition(1, beamImpactPosition);
            }

            Timing.CallDelayed(0.4f, () =>
            {
                for (int i = 0; i < _laserBeams.Length; ++i)
                {
                    _laserBeams[i].gameObject.SetActive(false);
                }
            }, gameObject);

            return impactOffset;
        }

        [Button]
        public void Shield()
        {
            DOTween.Kill(_shield);

            _shield.SetAlpha(0);
            _shield.gameObject.SetActive(true);

            DOTween.Sequence()
                    .Append(_shield.DOFade(1f, 0.05f).SetEase(Ease.OutFlash))
                    .AppendInterval(0.4f)
                    .Append(_shield.DOFade(0, 0.2f)).SetEase(Ease.InQuad)
                    .OnComplete(() =>
                    {
                        _shield.gameObject.SetActive(false);
                    });
        }

        internal void Dispose()
        {
            _shipController.EndCombat();
            GameObject.Destroy(gameObject);
        }

        [Button]
        public void TakeHullDamage(Vector3 offset)
        {
            if (tag == "Player")
            {
                _shipController.Combat.Screen.GameManager.ViewManager.ScreenShake();
            }

            _explossionEffect.T.localScale = Vector3.one;

            offset.z = _explossionEffect.T.localPosition.z;
            _explossionEffect.T.localPosition = offset;

            _explossionEffect.Play();
        }

        [Button]
        public void Destroy()
        {
            _renderer.gameObject.SetActive(false);

            if (tag == "Player")
            {
                _shipController.Combat.Screen.GameManager.ViewManager.BigScreenShake();
                _shipController.Combat.PlayerDestroyed();

                _explossionEffect.T.localScale = new Vector3(5f, 5f, 1f);
            }
            else
            {
                _shipController.Combat.EnemyDestroyed((EnemyShipController)_shipController);

                _explossionEffect.T.localScale = new Vector3(3f, 3f, 1f);
            }

            _explossionEffect.T.localPosition = new Vector3(0, 0, _explossionEffect.T.localPosition.z);
            _explossionEffect.Play();
        }
    }
}
