using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField, BoxGroup("Managers")]
        private LocationsManager _locationsManager;

        [SerializeField, BoxGroup("Managers")]
        private ViewManager _viewManager;

        [SerializeField, BoxGroup("Managers")]
        private PlayerShipModel _playerShipModel;

        [ShowInInspector, InlineEditor, HideInEditorMode, BoxGroup("Game State")]
        private PlayerShipController _player;

        private int _credits = 300;

        public LocationsManager LocationsManager => _locationsManager;

        public ViewManager ViewManager => _viewManager;

        public PlayerShipController Player => _player;

        public event Action OnCreditsChange;

        public int Credits {
            get => _credits;
            set
            {
                if (value != _credits)
                {
                    _credits = value;
                    OnCreditsChange?.Invoke();
                }
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            _locationsManager.Setup(this);

            _viewManager.Setup(this);
        }

        internal void StartGame()
        {
            _credits = 300;

            _player = new PlayerShipController(this, _playerShipModel);

            _locationsManager.Init();
            _viewManager.ShowMapScreen();
        }
    }
}