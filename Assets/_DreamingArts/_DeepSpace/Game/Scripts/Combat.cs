using System.Collections;
using System.Collections.Generic;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    [System.Serializable]
    public class Combat
    {
        private GameManager _gameManager;
        private CombatScreen _screen;

        private int _credits = 0;

        [ShowInInspector, InlineEditor]
        private List<EnemyShipController> _enemies;

        private List<Ship> _ships;

        public CombatScreen Screen => _screen;

        public int Credits => _credits;

        public Combat(GameManager gameManager, CombatScreen screen)
        {
            _gameManager = gameManager;
            _screen = screen;

            _ships = new List<Ship>();
            _enemies = _gameManager.LocationsManager.GetEnemies();

            for (int i = 0; i < _enemies.Count; ++i)
            {
                Ship enemyShip = Object.Instantiate(_enemies[i].Model.prefab, _screen.EnemiesPositions[i], Quaternion.identity);
                enemyShip.Setup(_enemies[i]);

                _ships.Add(enemyShip);

                _enemies[i].SetTarget(_gameManager.Player);
                _enemies[i].InitCombat(this, enemyShip);
            }

            Ship playerShip = Object.Instantiate(_gameManager.Player.Model.prefab, _screen.PlayerPosition, Quaternion.identity);
            playerShip.Setup(_gameManager.Player);

            _ships.Add(playerShip);

            _gameManager.Player.SetTarget(_enemies[0]);
            _gameManager.Player.InitCombat(this, playerShip);
        }

        public void PlayerDestroyed()
        {
            for (int i = 0; i < _enemies.Count; ++i)
            {
                _enemies[i].EndCombat();
            }

            _screen.GameOver();
        }

        public void EnemyDestroyed(EnemyShipController enemy)
        {
            _credits += (int)(enemy.CreditsReward * Random.Range(0.9f, 1.1f));
            _enemies.Remove(enemy);

            if (_enemies.Count == 0)
            {
                EndCombat();
            }
            else
            {
                _gameManager.Player.SetTarget(_enemies[0]);
            }
        }

        public void Dispose()
        {
            for (int i = 0; i < _ships.Count; ++i)
            {
                _ships[i].Dispose();
            }

            _ships.Clear();
        }

        private void EndCombat()
        {
            _gameManager.Player.EndCombat();
            _screen.EndCombat();
        }
    }
}