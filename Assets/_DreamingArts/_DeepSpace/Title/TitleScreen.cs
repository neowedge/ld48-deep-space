using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    public class TitleScreen : Screen
    {
        [SerializeField]
        private CanvasGroup _canvasGroup;

        [SerializeField]
        private GameManager _gameManager;

        private CoroutineHandle _checkInputCoroutine;

        private void Start()
        {
            Open();
        }

        public override bool Open()
        {
            if (base.Open())
            {
                _canvasGroup.gameObject.SetActive(true);
                _canvasGroup.DOFade(1f, 0.5f).OnComplete(() =>
                {
                    _checkInputCoroutine = Timing.CallContinuously(Mathf.Infinity, CheckInput);
                });
                
                return true;
            }
            return false;
        }

        public override bool Close()
        {
            if (_isOpen)
            {
                _isOpen = false;

                Timing.KillCoroutines(_checkInputCoroutine);

                _canvasGroup.DOFade(0, 0.5f).OnComplete(() =>
                {
                    _canvasGroup.gameObject.SetActive(false);
                    _gameManager.StartGame();
                });

                return true;
            }
            return false;
        }

        private void CheckInput()
        {
            if (Input.anyKeyDown)
            {
                Close();
            }
        }
    }
}