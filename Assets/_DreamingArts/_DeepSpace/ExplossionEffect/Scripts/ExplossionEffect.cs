﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace com.DreamingArts.DeepSpace
{
    public class ExplossionEffect : WorldActor
    {
        [SerializeField]
        SpriteRenderer _core;

        [SerializeField]
        SpriteRenderer _wave;

        public void Play(System.Action onEnd = null)
        {
            _core.color = new Color(
                    _core.color.r,
                    _core.color.g,
                    _core.color.b,
                    0);

            _wave.color = new Color(
                    _wave.color.r,
                    _wave.color.g,
                    _wave.color.b,
                    1f);

            Transform waveTransform = _wave.transform;
            waveTransform.localScale = Vector3.zero;

            _core.gameObject.SetActive(true);
            _wave.gameObject.SetActive(true);

            DOTween.Sequence()
                    .Append(_core.DOFade(1f, 0.5f).SetEase(Ease.OutFlash))
                    .Join(waveTransform.DOScale(1.5f, 0.5f).SetEase(Ease.OutSine))
                    .Join(_wave.DOFade(0f, 0.7f).SetEase(Ease.InExpo))
                    .OnComplete(() =>
                    {
                        _core.gameObject.SetActive(false);
                        _wave.gameObject.SetActive(false);

                        if (onEnd != null)
                        {
                            onEnd();
                        }
                    });
        }
    }
}