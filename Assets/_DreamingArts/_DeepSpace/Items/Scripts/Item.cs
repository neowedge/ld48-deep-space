using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.DeepSpace
{
    [CreateAssetMenu(fileName = "Item", menuName = "Deep Space/Item")]
    public class Item : ScriptableObject
    {
        public enum EItemType
        {
            Credits,
            HullRepair,
            HullUpgrade,
            ShieldUpgrade,
            ShieldRechargeSpeedUpgrade,
            ShieldRechargeCooldownUpgrade,
            AttackDamageUpgrade,
            AttackCooldownUpgrade,
            HyperspaceCooldownUpgrade
        }

        [SerializeField]
        private EItemType _type;

        [SerializeField]
        private Color _color = Color.white;

        [SerializeField]
        private int _basePrice;

        private int _value;

        public Color Color  => _color;

        public void SetValue(int value)
        {
            _value = value;
        }

        public float GetValue(GameManager gameManager, PlayerShipController player)
        {
            switch (_type)
            {
                case EItemType.Credits:
                    return _value;

                case EItemType.HullRepair:
                    return GetPrice(gameManager, player) / _basePrice;

                case EItemType.HullUpgrade:
                    return player.Model.hullLevelIncrement;

                case EItemType.ShieldUpgrade:
                    return player.Model.shieldLevelIncrement;

                case EItemType.ShieldRechargeSpeedUpgrade:
                    return player.Model.shieldLevelIncrement;

                case EItemType.ShieldRechargeCooldownUpgrade:
                    return player.Model.shieldRechargeCooldownLevelImprove;

                case EItemType.AttackDamageUpgrade:
                    return player.Model.attackDamageLevelIncrement;

                case EItemType.AttackCooldownUpgrade:
                    return player.Model.attackCooldownLevelImprove;

                case EItemType.HyperspaceCooldownUpgrade:
                    return player.Model.hyperspaceCooldownLevelImprove;
            }

            return 0;
        }

        public string GetTitle()
        {
            return I2.Loc.LocalizationManager.GetTranslation(_type.ToString() + "_Name");
        }

        public string GetDescription(GameManager gameManager, PlayerShipController player)
        {
            switch (_type)
            {
                case EItemType.Credits:
                    return string.Format(
                            I2.Loc.LocalizationManager.GetTranslation(_type.ToString() + "_Description"),
                            GetValue(gameManager, player));

                case EItemType.HullRepair:
                    return string.Format(
                            I2.Loc.LocalizationManager.GetTranslation(_type.ToString() + "_Description"),
                            GetValue(gameManager, player));

                case EItemType.HullUpgrade:
                    return string.Format(
                            I2.Loc.LocalizationManager.GetTranslation(_type.ToString() + "_Description"),
                            GetValue(gameManager, player));

                case EItemType.ShieldUpgrade:
                    return string.Format(
                            I2.Loc.LocalizationManager.GetTranslation(_type.ToString() + "_Description"),
                            GetValue(gameManager, player));

                case EItemType.ShieldRechargeSpeedUpgrade:
                    return string.Format(
                            I2.Loc.LocalizationManager.GetTranslation(_type.ToString() + "_Description"),
                            GetValue(gameManager, player));

                case EItemType.ShieldRechargeCooldownUpgrade:
                    return string.Format(
                            I2.Loc.LocalizationManager.GetTranslation(_type.ToString() + "_Description"),
                            GetValue(gameManager, player) * 100);

                case EItemType.AttackDamageUpgrade:
                    return string.Format(
                            I2.Loc.LocalizationManager.GetTranslation(_type.ToString() + "_Description"),
                            GetValue(gameManager, player));

                case EItemType.AttackCooldownUpgrade:
                    return string.Format(
                            I2.Loc.LocalizationManager.GetTranslation(_type.ToString() + "_Description"),
                            GetValue(gameManager, player) * 100);

                case EItemType.HyperspaceCooldownUpgrade:
                    return string.Format(
                            I2.Loc.LocalizationManager.GetTranslation(_type.ToString() + "_Description"),
                            GetValue(gameManager, player) * 100);
            }

            return string.Empty;
        }

        public int GetPrice(GameManager gameManager, PlayerShipController player)
        {
            switch (_type)
            {
                case EItemType.HullRepair:
                    int price = _basePrice * (player.Hull - player.CurrentHull);
                    while (price > gameManager.Credits)
                    {
                        price -= _basePrice;
                    }

                    return price;

                case EItemType.HullUpgrade:
                    return _basePrice * player.HullLevel;

                case EItemType.ShieldUpgrade:
                    return _basePrice * player.ShieldLevel;

                case EItemType.ShieldRechargeSpeedUpgrade:
                    return _basePrice * player.ShieldRechargeSpeedLevel;

                case EItemType.ShieldRechargeCooldownUpgrade:
                    return _basePrice * player.ShieldRechargeCooldownLevel;

                case EItemType.AttackDamageUpgrade:
                    return _basePrice * player.AttackDamageLevel;

                case EItemType.AttackCooldownUpgrade:
                    return _basePrice * player.AttackCooldownLevel;

                case EItemType.HyperspaceCooldownUpgrade:
                    return _basePrice * player.HyperspaceCooldownLevel;
            }

            return 0;
        }

        public void Take(GameManager gameManager, PlayerShipController player)
        {
            switch (_type)
            {
                case EItemType.Credits:
                    gameManager.Credits += (int)GetValue(gameManager, player);
                    break;

                case EItemType.HullRepair:
                    player.CurrentHull += (int)GetValue(gameManager, player);
                    break;

                case EItemType.HullUpgrade:
                    ++player.HullLevel;
                    player.CurrentHull += player.Model.hullLevelIncrement;
                    break;

                case EItemType.ShieldUpgrade:
                    ++player.ShieldLevel;
                    break;

                case EItemType.ShieldRechargeSpeedUpgrade:
                    ++player.ShieldRechargeSpeedLevel;
                    break;

                case EItemType.ShieldRechargeCooldownUpgrade:
                    ++player.ShieldRechargeCooldownLevel;
                    break;

                case EItemType.AttackDamageUpgrade:
                    ++player.AttackDamageLevel;
                    break;

                case EItemType.AttackCooldownUpgrade:
                    ++player.AttackCooldownLevel;
                    break;

                case EItemType.HyperspaceCooldownUpgrade:
                    ++player.HyperspaceCooldownLevel;
                    break;
            }
        }
    }
}